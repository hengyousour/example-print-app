import 'package:block_ui/block_ui.dart';
import 'package:example_print_app/screens/simple_receipt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_printer/flutter_bluetooth_printer.dart';
import 'package:provider/provider.dart';
import 'package:quickalert/quickalert.dart';
import 'package:screenshot/screenshot.dart';

import '../providers/config_printer_provider.dart';

class PrintPreview extends StatefulWidget {
  const PrintPreview({super.key});

  @override
  State<PrintPreview> createState() => _PrintPreviewState();
}

class _PrintPreviewState extends State<PrintPreview> {
  late ConfigPrinterProvider readProvider;
  late String args = '';
  @override
  void initState() {
    super.initState();
    readProvider = context.read<ConfigPrinterProvider>();
  }

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = TextButton.styleFrom(
        foregroundColor: Theme.of(context).colorScheme.onPrimary);
    // Extract the arguments from the current ModalRoute
    // settings and cast them as ScreenArguments.
    args = ModalRoute.of(context)?.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Print Preview'),
        actions: [
          TextButton(
              onPressed: () async {
                if (args == 'bluetooth') {
                  readProvider.printEvent(context: context).then((res) {
                    BlockUi.hide(context);
                    QuickAlert.show(
                        context: context,
                        type: QuickAlertType.success,
                        text: res,
                        confirmBtnColor: Colors.white,
                        confirmBtnTextStyle:
                            const TextStyle(color: Colors.black));
                  }).onError((error, stackTrace) {
                    QuickAlert.show(
                        context: context,
                        type: QuickAlertType.error,
                        text: '$error',
                        confirmBtnColor: Colors.white,
                        confirmBtnTextStyle:
                            const TextStyle(color: Colors.black));
                  });
                }

                if (args == 'wifi-network') {
                  readProvider
                      .printByIpAddressEvent(context: context)
                      .then((res) {
                    BlockUi.hide(context);
                    QuickAlert.show(
                        context: context,
                        type: QuickAlertType.success,
                        text: res,
                        confirmBtnColor: Colors.white,
                        confirmBtnTextStyle:
                            const TextStyle(color: Colors.black));
                  }).onError((error, stackTrace) {
                    QuickAlert.show(
                        context: context,
                        type: QuickAlertType.error,
                        text: '$error',
                        confirmBtnColor: Colors.white,
                        confirmBtnTextStyle:
                            const TextStyle(color: Colors.black));
                  });
                }
              },
              style: style,
              child: const Text(
                'Print',
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if (args == 'bluetooth')
              Receipt(
                backgroundColor: Colors.transparent,
                builder: (_) => const SimpleReceipt(),
                onInitialized: ((controller) =>
                    readProvider.receiptController = controller),
              ),
            if (args == 'wifi-network')
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Center(
                  child: Screenshot(
                      controller: readProvider.screenshotController,
                      child: const SimpleReceipt(
                          connectionType: ConnectionType.ipAddress)),
                ),
              )
          ],
        ),
      ),
    );
  }
}
