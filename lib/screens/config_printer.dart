import 'package:example_print_app/providers/config_printer_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_printer/flutter_bluetooth_printer.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:quickalert/quickalert.dart';

class ConfigPrinter extends StatefulWidget {
  const ConfigPrinter({super.key});

  @override
  State<ConfigPrinter> createState() => _ConfigPrinterState();
}

class _ConfigPrinterState extends State<ConfigPrinter> {
  final formConfigPrinterKey = GlobalKey<FormBuilderState>();
  late ConfigPrinterProvider readProivder;
  late Future<Map<String, dynamic>> asyncMethods;

  @override
  void initState() {
    super.initState();
    readProivder = context.read<ConfigPrinterProvider>();
    asyncMethods = readProivder.initData();
  }

  ReceiptController? controller;
  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = TextButton.styleFrom(
        foregroundColor: Theme.of(context).colorScheme.onPrimary);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Config  Printer'),
        actions: [
          TextButton(
            onPressed: () {
              if (formConfigPrinterKey.currentState!.saveAndValidate()) {
                readProivder
                    .saveEvent(
                        formDoc: formConfigPrinterKey.currentState!.value)
                    .then((res) {
                  QuickAlert.show(
                      context: context,
                      type: QuickAlertType.success,
                      text: 'configuration is saved.',
                      confirmBtnColor: Colors.white,
                      confirmBtnTextStyle:
                          const TextStyle(color: Colors.black));
                });
              }
            },
            style: style,
            child: const Text('Save',
                style: TextStyle(fontWeight: FontWeight.bold)),
          )
        ],
      ),
      body: FutureBuilder(
          future: asyncMethods,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const CircularProgressIndicator();
            }
            Map<String, dynamic> data = snapshot.data ?? {};
            return SingleChildScrollView(
              padding: const EdgeInsets.all(10.0),
              child: FormBuilder(
                key: formConfigPrinterKey,
                initialValue: {
                  'printerIpAddress': data['printerIpAddress'],
                  'printerPort': data['printerPort'] ?? '9100',
                  'bluetoothPrinterName': data['btPrinterName'],
                  'bluetoothPrinterAddress': data['btPrinterAddress'],
                  'bluetoothPrinterType': data['btPrinterType'].toString()
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: FormBuilderTextField(
                            name: 'printerIpAddress',
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Printer IP Address',
                            ),
                          ),
                        ),
                        const SizedBox(width: 10.0),
                        Expanded(
                          child: FormBuilderTextField(
                            name: 'printerPort',
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Port',
                            ),
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required()]),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10.0),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: FormBuilderTextField(
                            name: 'bluetoothPrinterName',
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Bluetooth Printer  Name',
                            ),
                          ),
                        ),
                        const SizedBox(width: 10.0),
                        ElevatedButton(
                            onPressed: () async {
                              final BluetoothDevice? selectedBluetoothDevice =
                                  await FlutterBluetoothPrinter.selectDevice(
                                      context);
                              if (selectedBluetoothDevice != null) {
                                formConfigPrinterKey.currentState!.patchValue({
                                  'bluetoothPrinterName':
                                      selectedBluetoothDevice.name,
                                  'bluetoothPrinterAddress':
                                      selectedBluetoothDevice.address
                                });
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                padding: const EdgeInsets.all(17.0),
                                elevation: 0.0,
                                shadowColor: Colors.transparent),
                            child: const Icon(Icons.bluetooth))
                      ],
                    ),
                    Visibility(
                      maintainState: true,
                      visible: false,
                      child: Row(
                        children: [
                          Expanded(
                            child: FormBuilderTextField(
                              name: 'bluetoothPrinterAddress',
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Bluetooth Printer Address',
                              ),
                            ),
                          ),
                          // Expanded(
                          //   child: FormBuilderTextField(
                          //     name: 'bluetoothPrinterType',
                          //     decoration: const InputDecoration(
                          //       border: OutlineInputBorder(),
                          //       labelText: 'Bluetooth Printer Type',
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 10.0),
                  ],
                ),
              ),
            );
          }),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(children: [
          Expanded(
            child: ElevatedButton(
                onPressed: () {
                  readProivder.resetEvent().then((res) {
                    formConfigPrinterKey.currentState!.patchValue({
                      'printerIpAddress': '',
                      'printerPort': '9100',
                      'bluetoothPrinterName': '',
                      'bluetoothPrinterAddress': '',
                    });
                    QuickAlert.show(
                        context: context,
                        type: QuickAlertType.success,
                        text: 'configuration is reset.',
                        confirmBtnColor: Colors.white,
                        confirmBtnTextStyle:
                            const TextStyle(color: Colors.black));
                  });
                },
                style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.all(17.0)),
                child: const Text('Reset')),
          ),
        ]),
      ),
    );
  }
}
