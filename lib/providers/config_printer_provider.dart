import 'dart:typed_data';

import 'package:block_ui/block_ui.dart';
import 'package:example_print_app/storages/printer_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_printer/flutter_bluetooth_printer.dart';
import 'package:esc_pos_printer/esc_pos_printer.dart';
// ignore: depend_on_referenced_packages
import 'package:esc_pos_utils/esc_pos_utils.dart' as esc;
// ignore: depend_on_referenced_packages
import 'package:image/image.dart' as i;
import 'package:screenshot/screenshot.dart';

class ConfigPrinterProvider with ChangeNotifier {
  //Bluetooth Printer
  ReceiptController? receiptController;
  //Wifi/Network printer
  ScreenshotController screenshotController = ScreenshotController();

  Future<Map<String, dynamic>> initData() async {
    Map<String, dynamic> wnPrinterDoc = await PrinterStorage().getWNPrinter();
    Map<String, dynamic> btPrinterDoc = await PrinterStorage().getPrinter();
    Map<String, dynamic> data = {
      'printerIpAddress': wnPrinterDoc['address'],
      'printerPort': wnPrinterDoc['port'],
      'btPrinterName': btPrinterDoc['name'],
      'btPrinterAddress': btPrinterDoc['address'],
    };

    return data;
  }

  Future<String> printEvent({required BuildContext context}) async {
    Map<String, dynamic> btPrinterDoc = await PrinterStorage().getPrinter();
    String btPrinterAddress = btPrinterDoc['address'] ?? '';
    //check printer address exist or not
    if (btPrinterAddress.isNotEmpty) {
      if (context.mounted) BlockUi.show(context);
      //set paper size
      receiptController!.paperSize = PaperSize.mm58;
      //start printing
      await receiptController!.print(address: btPrinterAddress, linesAfter: 1);
      return Future.value('Good Job, Printing is Completed.');
    } else {
      return Future.error("Printer not found.");
    }
  }

  Future<String> printByIpAddressEvent({required BuildContext context}) async {
    Map<String, dynamic> wnPrinterDoc = await PrinterStorage().getWNPrinter();
    String printerIpAddress = wnPrinterDoc['address'] ?? '';
    String printerPort = wnPrinterDoc['port'] ?? '';
    const esc.PaperSize paper = esc.PaperSize.mm80;
    final profile = await esc.CapabilityProfile.load();
    final printer = NetworkPrinter(paper, profile);
    // check printer ip address and port exist or not
    if (printerIpAddress.isEmpty || printerPort.isEmpty) {
      return Future.error('Printer not found.');
    }
    // connect printer by ip address
    final PosPrintResult res =
        await printer.connect(printerIpAddress, port: int.parse(printerPort));
    if (res == PosPrintResult.success) {
      if (context.mounted) BlockUi.show(context);
      Uint8List? capturedImage =
          await screenshotController.capture(pixelRatio: 2.0);
      await _startPrint(printer, capturedImage!);
      return Future.value('Good Job, Printing is Completed.');
    } else {
      return Future.error(res.msg);
    }
  }

  Future<void> _startPrint(NetworkPrinter printer, Uint8List img) async {
    final i.Image? image = i.decodeImage(img);
    printer.image(image!, align: esc.PosAlign.center);
    printer.cut();
    printer.disconnect();
  }

  Future<void> saveEvent({required Map<String, dynamic> formDoc}) async {
    //set wifi/network or bluetooth printer doc to local storage
    PrinterStorage().setWNPrinter(
        address: formDoc['printerIpAddress'] ?? '',
        port: formDoc['printerPort']);
    PrinterStorage().setPrinter(
      name: formDoc['bluetoothPrinterName'] ?? '',
      address: formDoc['bluetoothPrinterAddress'] ?? '',
    );
  }

  Future<void> resetEvent() async {
    return PrinterStorage().clearPrinter();
  }
}
