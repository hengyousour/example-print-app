import 'package:flutter/material.dart';
import 'widgets/drawer.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Example Print App'),
      ),
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10.0),
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          ElevatedButton.icon(
            onPressed: () {
              Navigator.of(context)
                  .pushNamed('/print-preview', arguments: 'bluetooth');
            },
            label: const Text('Print With Bluetooth Printer'),
            icon: const Icon(Icons.bluetooth),
          ),
          ElevatedButton.icon(
              onPressed: () {
                Navigator.of(context)
                    .pushNamed('/print-preview', arguments: 'wifi-network');
              },
              label: const Text('Print With Wifi / Network Printer'),
              icon: const Icon(Icons.settings_ethernet))
        ]),
      ),
    );
  }
}
