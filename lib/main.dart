import 'package:example_print_app/screens/print_preview.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'app.dart';
import 'providers/config_printer_provider.dart';
import 'screens/config_printer.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (ctx) => ConfigPrinterProvider(),
      ),
    ],
    child: MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => const App(),
        '/config-printer': (context) => const ConfigPrinter(),
        '/print-preview': (context) {
          return const PrintPreview();
        }
      },
    ),
  ));
}
