import 'package:shared_preferences/shared_preferences.dart';

class PrinterStorage {
  // Create SharedPreferences
  final Future<SharedPreferences> _printerPrefs =
      SharedPreferences.getInstance();

  //Bluetooth Priter
  void setPrinter({
    required String name,
    required String address,
    // required int type,
  }) async {
    SharedPreferences printerPrefs = await _printerPrefs;
    printerPrefs.setString('name', name);
    printerPrefs.setString('address', address);
    // printerPrefs.setInt('type', type);
  }

  Future<Map<String, dynamic>> getPrinter() async {
    SharedPreferences printerPrefs = await _printerPrefs;
    return {
      'name': printerPrefs.getString('name'),
      'address': printerPrefs.getString('address'),
      // 'type': printerPrefs.getInt('type')
    };
  }

  //Wifi & Network Printer
  void setWNPrinter({required String address, required String port}) async {
    SharedPreferences printerPrefs = await _printerPrefs;
    printerPrefs.setString('printerAddress', address);
    printerPrefs.setString('printerPort', port);
  }

  Future<Map<String, dynamic>> getWNPrinter() async {
    SharedPreferences printerPrefs = await _printerPrefs;
    return {
      'address': printerPrefs.getString('printerAddress'),
      'port': printerPrefs.getString('printerPort'),
    };
  }

  void clearPrinter() async {
    SharedPreferences printerPrefs = await _printerPrefs;
    //Bluetooth Printer
    printerPrefs.remove('name');
    printerPrefs.remove('address');
    // printerPrefs.remove('type');
    //Wifi & Network Printer
    printerPrefs.remove('printerAddress');
    printerPrefs.remove('printerPort');
  }
}
